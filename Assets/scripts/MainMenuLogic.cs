﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuLogic : MonoBehaviour {

	public GameObject mainPanel;
	public GameObject helpPanel;
	public GameObject settingPanel;
	public GameObject loadingPanel;
	public GameObject creditPanel;

	void Awake(){
        Time.timeScale = 1;
		CanvasActive (true, false, false,false,false);
	}

	void Start () {
	
	}

	void Update () {

	}

    public void PlayMainMenu()
    {
        CanvasActive(false, true, false, false, false);
        Debug.Log("LAST LEVEL PLAYER PREFS: "+PlayerPrefs.GetString("LAST_LVL"));
        SceneManager.LoadSceneAsync(PlayerPrefs.GetString("LAST_LVL"));
    }

	public void Help(){
		CanvasActive (false, false, true, false, false);
	}

	public void Settings(){
		CanvasActive (false, false, false, true, false);
	}

	public void Credits (){
		CanvasActive (false, false, false, false, true);
	}

	public void ExitGame(){
		Application.Quit ();
	}

	public void GoToMainMenu(){
		CanvasActive (true, false, false, false, false);
	}

	public void LoadScenesWithLoadingScreen(string sceneName){
		CanvasActive (false, true,false,false,false);
		SceneManager.LoadSceneAsync (sceneName);
	}

    public void CloseSesion() {
        CanvasActive(false, true, false, false, false);
        PlayerPrefs.SetInt("LOGGED", 0);
        SceneManager.LoadSceneAsync("lvl_login");
    }

	public void CanvasActive(bool main,bool loadingCanvas,bool help,bool setings,bool credit){
		mainPanel.SetActive(main);
		helpPanel.SetActive(help);
		settingPanel.SetActive(setings);
		loadingPanel.SetActive(loadingCanvas);
		creditPanel.SetActive (credit);
	}
		
}
