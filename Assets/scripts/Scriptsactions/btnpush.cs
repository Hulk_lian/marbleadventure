﻿using UnityEngine;
using System.Collections;

public class btnpush : MonoBehaviour {

	bool _btnPushed;
	public Animator _animatorBtn;
	public Animator _animatorWall;

	void Start () {
		_btnPushed = false;
	}

	void Update () {
		
	}

	void OnCollisionEnter(Collision collision){
		_btnPushed = true;
		PushAnimation ();
	}

	void PushAnimation(){
		if (_btnPushed) {
			_animatorBtn.SetBool ("push", true);
			_animatorWall.SetBool ("destroyWall", true);
		}
	}
}
