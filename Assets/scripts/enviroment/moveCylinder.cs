﻿using UnityEngine;
using System.Collections;

public class moveCylinder : MonoBehaviour {
	
	public float speed=1.0f;
	public float time = 0.01f;

	void Start () {
		StartCoroutine (MoveWallC ());
	}

	void Update () {
	}

	IEnumerator MoveWallC(){
		while (true) {
			transform.Rotate (new Vector3 (0, speed, 0) * Time.deltaTime);
			yield return new WaitForSeconds (time);
		}
	}
}
