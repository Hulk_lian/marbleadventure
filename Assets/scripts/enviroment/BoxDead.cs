﻿using UnityEngine;
using System.Collections;

public class BoxDead : MonoBehaviour {

	public UIGame uigame;

	void Awake(){
        Time.timeScale = 1;
	}

	void Start () {
	
	}

	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		//dead player
		if (other.tag == "Player") {
			other.gameObject.GetComponent<Rigidbody> ().isKinematic = true;
            Time.timeScale = 0;
			//confirmation
			uigame.CanvasActive (false, false, true);
		} else
			GameObject.Destroy(other.gameObject);

	}


}
