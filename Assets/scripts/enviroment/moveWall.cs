﻿using UnityEngine;
using System.Collections;

public class moveWall : MonoBehaviour {

	public Vector3 pointA,pointB;
	public float speed=1.0f;
	public float time = 0.01f;
	public int lengthPing;

	void Start () {
		StartCoroutine (MoveWallC ());
	}

	void Update () {
	}

	IEnumerator MoveWallC(){
		while (true) {
			float pos = Mathf.PingPong (Time.time * speed, lengthPing);
			transform.position = Vector3.Lerp (pointA, pointB,pos);
			yield return new WaitForSeconds (time);
		}
	}
}
