﻿using UnityEngine;
using System.Collections;

public class FloorFalling : MonoBehaviour {

	public GameObject floor;
	public float timeFall = 2f;

	void Start () {
	
	}

	void Update () {
	
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player") {
			StartCoroutine (WaitPLS());
		}
	}

	IEnumerator WaitPLS(){
		yield return new WaitForSeconds(timeFall);
		floor.AddComponent<Rigidbody>();
	}
}
