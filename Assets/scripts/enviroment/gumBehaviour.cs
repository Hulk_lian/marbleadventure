﻿using UnityEngine;
using System.Collections;

public class gumBehaviour : MonoBehaviour {

	//Constants
	private const float ANGSLOW=10;

	public GameObject _player;
	private float _forcePlayer;
	private float _angularDragBK;//bakup angular drag
	public float _forceReduction=0.6f;


	void Start () {
		_player = GameObject.FindGameObjectWithTag ("Player");
		_forcePlayer = _player.GetComponent<PJControl>().GetForce();
		_angularDragBK = _player.GetComponent<Rigidbody> ().angularDrag;
	}

	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject==_player) {
			ChangeSpeed ((_forcePlayer*_forceReduction),ANGSLOW);
		}
	}
	void OnTriggerExit(Collider other) {
		if (other.gameObject ==_player) {
			ChangeSpeed (_forcePlayer,_angularDragBK);
		}
	}

	void ChangeSpeed(float newForce,float angularDrag){
		_player.GetComponent<PJControl> ().SetForce (newForce);
		_player.GetComponent<Rigidbody> ().angularDrag = angularDrag;
	}
}
