﻿using UnityEngine;
using System.Collections;

public class BouncyCylinder : MonoBehaviour {

	public float force =3;//force is how forcefully push the player away.
	public Rigidbody _rigidbody;
	public Transform _transform;

	void Start () {
	
	}

	void Update () {
	
	}

	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag == "Player") {
			//calculate Angle
			Vector3 dir=col.contacts[0].point -_transform.position;
			dir = -dir.normalized;
			_rigidbody.AddForce (dir * force);

		}
			
	}
}
