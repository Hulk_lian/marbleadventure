﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadTextCharger : MonoBehaviour {

    public Text[] objectwithtext;
    public Text howToPlay;
    public Text credits;
    LanguageManager lngm;
    TextAsset txtTextAssertHelp,txtTextAssertCredits;
    
    void Awake() {
        lngm=LanguageManager.Load();
        ReadLang();
        lngm.SetLanguage(ReadLang());
        foreach (Text t in objectwithtext){
            t.text = lngm.GetString(t.text);
        }
        if(ReadLang()==Language.Spanish){
            txtTextAssertHelp = (TextAsset)Resources.Load("howToPlayES", typeof(TextAsset));
            txtTextAssertCredits = (TextAsset)Resources.Load("CreditsES", typeof(TextAsset));
        }else{
            txtTextAssertHelp = (TextAsset)Resources.Load("howToPlayEN", typeof(TextAsset));
            txtTextAssertCredits = (TextAsset)Resources.Load("CreditsEN", typeof(TextAsset));
        }
        if(SceneManager.GetActiveScene().name.Contains("main_menu")){
            howToPlay.text = txtTextAssertHelp.text;
            credits.text = txtTextAssertCredits.text;
        }
    }

    private Language ReadLang()
    {
        Language lang;
        switch (Application.systemLanguage.ToString())
        {
            case "Spanish":
                lang = Language.Spanish;
                break;

            case "English":
                lang = Language.English;
                break;

            default:
                lang = Language.Spanish;
                break;
        }
        return lang;
    }
}
