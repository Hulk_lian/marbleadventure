﻿using UnityEngine;
using System.Collections;

public class PlayerStates : MonoBehaviour {

	//Drunk Mode
	public GameObject drunkEfect;

	void Start () {
		drunkEfect.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider colider){
		if (colider.gameObject.tag=="Player") {
			drunkEfect.SetActive(true);
		}
	}
}
