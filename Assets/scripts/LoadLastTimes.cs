﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadLastTimes : MonoBehaviour {

    public Text[] textlvls;
    private const string baseTime = "00:00:00";
    private string baseTimePref = "lvl_";

    void Awake() {
        Debug.Log(PlayerPrefs.GetString("MailLogged"));
        int inthelp = 0;
        for (int i = 0; i < textlvls.Length; i++){
            inthelp = i + 1;
            Debug.Log("INTHELP= " + inthelp.ToString() + "   I " + i.ToString() + "   " + baseTimePref + inthelp.ToString());
            if (PlayerPrefs.HasKey(baseTimePref + inthelp.ToString() + "#" + PlayerPrefs.GetString("MailLogged")))
            {
                textlvls[i].text = PlayerPrefs.GetString(baseTimePref + inthelp.ToString()+"#"+PlayerPrefs.GetString("MailLogged"));
            }
            else {
                textlvls[i].text = baseTime;
            }

        }
    }
}
