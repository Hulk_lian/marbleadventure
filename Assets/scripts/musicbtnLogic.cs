﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicbtnLogic : MonoBehaviour {

    public bool music;
    string key_music = "MUSIC";
    public GameObject sounds;
    public GameObject buttonStrike;

    private void Awake(){

        Screen.sleepTimeout = (int)0f;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        if (PlayerPrefs.GetInt(key_music) == 1)
            music = true;
        else
            music = false;
    }

    void Start () {
        buttonStrike.SetActive(!GetMusicValue());
        CheckMusic();
    }
	
	void Update () {
		
	}

    public void ClickMusicButton() {
        InverseMusicValue();
        Debug.Log("MUSIC BTN CLICK "+GetMusicValue());
        CheckMusic();
    }
    private void CheckMusic()
    {
        this.buttonStrike.SetActive(!GetMusicValue());
        this.sounds.SetActive(GetMusicValue());
    }

    public void SetMusicValue(bool music)
    {
        this.music = music;
        WriteMusicConfigInPrefs();
    }

    public bool GetMusicValue()
    {
        return this.music;
    }
    public void InverseMusicValue()
    {
        Debug.Log("InverseValue");
        this.music = !this.music;
        WriteMusicConfigInPrefs();
    }
    public void WriteMusicConfigInPrefs()
    {
        Debug.Log("WRITE MUSIC IN CONFIG");
        if (music)
        {
            PlayerPrefs.SetInt(key_music, 1);
            Debug.Log("MUSiC  " + music + "  PLAYER PREFS: " + PlayerPrefs.GetInt(key_music));
        }
        else
        {
            PlayerPrefs.SetInt(key_music, 0);
            Debug.Log("MUSiC  " + music + "  PLAYER PREFS: " + PlayerPrefs.GetInt(key_music));
        }
    }


}
