﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;

[XmlRoot("LanguageManager")]
public class LanguageManager{

    private static string g_filePath = Path.Combine(Application.dataPath, "Resources/Languages.xml");
    public static TextAsset textAssert = (TextAsset)Resources.Load("Languages", typeof(TextAsset));

    [XmlEnum("CurrentLanguage")]
    public Language _currentLanguage; //Current Language - set a the beginin of the game (1st time)

    [XmlArray("Translations"),XmlArrayItem("Translation")]
    public List<Translation> _translations;//List of Translation

    public LanguageManager(){
        _currentLanguage = Language.English;
        _translations = new List<Translation>();
    }

    public void SetLanguage(Language lang){
        _currentLanguage = lang;
    }

    public bool ContainsKay(string key){
        return FindEntry(key) >= 0;
    }

    public void AddTranslation(Translation translation){
        Insert(translation, true);
    }

    public void UpdateTranslation(Translation translation) {
        Insert(translation, false);
    }

    public void RemoveTranslation(Translation translation) {
        _translations.Remove(translation);
    }

    public void RemoveTranslation(int index)
    {
        _translations.RemoveAt(index);
    }

    public void RemoveTranslation(string key)
    {
        int i = FindEntry(key);
        if(i>=0)
           RemoveTranslation(i);
    }

    public string GetString(string key){
        int i=FindEntry(key);

        if (i >= 0) 
            return _translations [i].GetValue(_currentLanguage);

        return string.Empty;

    }

    public string GetString(string key,Language language){
        int i = FindEntry(key);

        if (i >= 0)
            return _translations[i].GetValue(language);

        return string.Empty;
    }

    private void Insert(Translation translation, bool add) {
        int i = FindEntry(translation.key);
        if (i >= 0){
            if (add)
                return;
            _translations[i] = translation;
        }

        if (add) {
            _translations.Add(translation);
        }
    }

    private int FindEntry(string key) {
        for (int i = 0; i < _translations.Count; i++){
            if (_translations[i].key.Equals(key))
                return i;
        }
        return -1;
    }

    public void Save() {
        XmlSerializer serializer = new XmlSerializer(typeof(LanguageManager));
        using (Stream stream = new FileStream(g_filePath, FileMode.Create)) {
            serializer.Serialize(stream, this);
        }
    }

    public static LanguageManager Load()
    {
        //#if UNITY_ANDROID
                XmlSerializer serializer = new XmlSerializer(typeof(LanguageManager));
                using (StringReader stream = new StringReader(textAssert.text))
                {
                    return serializer.Deserialize(stream) as LanguageManager;
                } 
       // #endif

       /* #if UNITY_EDITOR
            if (File.Exists(g_filePath)){
                XmlSerializer serializerE = new XmlSerializer(typeof(LanguageManager));
                using (Stream stream = new FileStream(g_filePath, FileMode.Open))
                {
                    return serializerE.Deserialize(stream) as LanguageManager;
                }
            }
            return new LanguageManager();
        #endif*/
    }

}

public enum Language
{
    English,
    Spanish
}
