﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIGame : MonoBehaviour {

	public GameObject _loadingCanvas;
	public GameObject _gameCanvas;
	public GameObject _retryCanvas;

	private const string MAIN_MENU_SCENE_NAME="main_menu";

	void Awake(){
        Time.timeScale = 1;
		CanvasActive (true, false, false);
	}
		
	public void Play(){
		LoadScenesWithLoadingScreen ("lvl_1");
	}
		
	public void GoToMainMenu(){
        Time.timeScale = 0;
		LoadScenesWithLoadingScreen (MAIN_MENU_SCENE_NAME);
	}

	public void NextLVL(string lvlname){
		LoadScenesWithLoadingScreen (lvlname);
	}

    public void RepeatLVL(){
        LoadScenesWithLoadingScreen(SceneManager.GetActiveScene().name);
    }

	private void LoadScenesWithLoadingScreen(string sceneName){
		CanvasActive (false, true,false);
		SceneManager.LoadSceneAsync (sceneName);
	}

    public void Share()
    {

    }

	public void CanvasActive(bool gameCanvas,bool loadingCanvas,bool retryCanvas){
		_loadingCanvas.SetActive(loadingCanvas);
		_gameCanvas.SetActive (gameCanvas);
		_retryCanvas.SetActive (retryCanvas);
	}

}
