﻿using System.Xml.Serialization;

public class Translation{

    [XmlAttribute("key")]
    public string key;
    
    [XmlAttribute("english")]
    public string english;
    [XmlAttribute("spanish")]
    public string spanish;

    public Translation()
    {
        key = "New Key";
        english = string.Empty;
        spanish = string.Empty;

    }

    public string GetValue(Language language) {
        string res = "";
        switch (language) { 
            case Language.English:
                res=english;
                break;
            case Language.Spanish:
                res=spanish;
                break;
            default:
                res = spanish;
                break;
        }
        return res;
    }
}
