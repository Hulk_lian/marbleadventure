﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Game : MonoBehaviour {

    string key_music = "MUSIC";
    public GameObject sounds;
    public GameObject follower;
	public GameObject player;

    private void Awake(){
        string sceneName=SceneManager.GetActiveScene().name;
        Debug.Log("PLAYER PREF ESCRITURA NIVEL "+ sceneName);
        PlayerPrefs.SetString("LAST_LVL", sceneName);
    }

    void Start () {
        FollowPlayer ();
        ReadMusicConfigInPrefs();
	}

	void Update () {
		FollowPlayer ();
	}

	void FollowPlayer(){
		follower.transform.position = player.transform.position;
	}

    public void ReadMusicConfigInPrefs()
    {
        Debug.Log("READ MUSIC IN CONFIG");
        if (PlayerPrefs.GetInt(key_music) == 1)
            sounds.SetActive(true);
        else
            sounds.SetActive(false);
    }
}
