﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeLevel : MonoBehaviour {

	public Text textTime;
	private System.DateTime startTime;
	private System.TimeSpan timeNow;

	//crono
	private float sec;
	private float min;
	private float milis;

	void Awake() {
		textTime= GameObject.Find("LBL_time").GetComponent<Text>();
		ResetStarTime ();
	}

	void FixedUpdate(){
		timeNow = System.DateTime.Now.Subtract(startTime);
        string text_Time="";

        //text_Time = string.Format("{0:00}:{1:00}:{2:000}", timeNow.Minutes, timeNow.Seconds, timeNow.Milliseconds);

       // if (timeNow.Hours > 0)
            text_Time = string.Format("{0:00}:{1:00}:{2:00}", timeNow.Hours, timeNow.Minutes, timeNow.Seconds); //codifica el formato tiempo
        
         if(timeNow.Days>0)
            text_Time = string.Format("{0:00}d,{1:00}:{2:00}:{3:00}",timeNow.Days,timeNow.Hours, timeNow.Minutes, timeNow.Seconds); //codifica el formato tiempo
        textTime.text=text_Time;
	}

	public void ResetStarTime(){
		startTime = System.DateTime.Now;
		textTime.text = "00:00:000";		
	}
}