﻿using UnityEngine;
using System.Collections;

public class PJControl : MonoBehaviour {

    public Rigidbody _rigidbody;
    float _movementHorizontal;
    float _movementVertical;
	Vector3 movimiento;
	public float _force;	//fuerza de empuje

	void Start () {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;//never turn off the screen

		Input.compensateSensors=true;
		gameObject.GetComponent<Rigidbody> ().useGravity = true;
		Time.timeScale = 1;
	}

    void FixedUpdate(){
		
		#if UNITY_ANDROID		
		//Final
		_movementHorizontal = Input.acceleration.x;
		_movementVertical = Input.acceleration.y;
		#endif

		#if UNITY_EDITOR
		//mode debug
		_movementHorizontal = Input.GetAxis("Horizontal");
		_movementVertical = Input.GetAxis("Vertical");
		#endif

        movimiento = new Vector3(_movementHorizontal * _force * Time.deltaTime, 0, _movementVertical * _force * Time.deltaTime);

        _rigidbody.AddForce(movimiento, ForceMode.Force);

    }

	void Update () {

	}

	public float GetForce(){
		return this._force;
	}
	public void SetForce(float newForce){
		this._force = newForce;
	}
}
