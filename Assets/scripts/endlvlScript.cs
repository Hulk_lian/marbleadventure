﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class endlvlScript : MonoBehaviour {
	
	public Text _temporal;
	public GameObject _btnNext;

	TimeLevel _timelvl;

	void Start () {
		Time.timeScale = 1;

		_timelvl = gameObject.AddComponent<TimeLevel> ();

		//debug
		ShowEndGame(false,false);
	}

	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			Time.timeScale = 0;
			GetTimeText();
		}
	}

	void GetTimeText(){
		_temporal.text = _timelvl.textTime.text;
        Debug.Log(SceneManager.GetActiveScene().name + _temporal.text);
        PlayerPrefs.SetString(SceneManager.GetActiveScene().name+"#"+PlayerPrefs.GetString("MailLogged"),_temporal.text);
		ShowEndGame (true, true);
	}

	private void ShowEndGame(bool timeGame,bool buttonNext){
		_temporal.enabled=timeGame;
		_btnNext.SetActive(buttonNext);
	}
}
