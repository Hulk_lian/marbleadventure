﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Share : MonoBehaviour
{
    public string title;
    public Text textTime;
    public string lvlName;
    public string urlPS;
    static AndroidJavaClass sharePluginClass;

		void Start ()
		{
			 lvlName = SceneManager.GetActiveScene().name.Replace("lvl_", "");
				sharePluginClass = new AndroidJavaClass ("com.ari.tool.UnityAndroidTool");
				if (sharePluginClass == null) {
						Debug.Log ("sharePluginClass es nula");
				} else {
						Debug.Log ("sharePluginClass no es nula");
				}
		}

        public void ShareContent()
        {
            string content="";
            if (ReadLang() == Language.Spanish)
            {
                content = "He conseguido un tiempo de: " + textTime.text + " en el nivel " + lvlName + "."
                + " ¿Puedes hacerlo mejor? Intenta superarme jugando a Marble Adventure https://play.google.com/store/apps/details?id=com.jtcode.marbleadventure";
            }
            else
            {
                content = "I did the level " +lvlName +" in: "+ textTime.text+ 
                    " try to beat me playing Marble Adventure https://play.google.com/store/apps/details?id=com.jtcode.marbleadventure";
            }
            
            CallShare(title, "", content);
        }

		public static void CallShare (string handline, string subject, string text)
		{
				Debug.Log ("iniciamos la llamada a compartir");
				sharePluginClass.CallStatic ("share", new object[] {
						handline,
						subject,
						text
				});
				Debug.Log ("terminamos la llamada a compartir");
		}

        private Language ReadLang()
        {
            Language lang;
            switch (Application.systemLanguage.ToString())
            {
                case "Spanish":
                    lang = Language.Spanish;
                    break;

                case "English":
                    lang = Language.English;
                    break;

                default:
                    lang = Language.Spanish;
                    break;
            }
            return lang;
        }
}
